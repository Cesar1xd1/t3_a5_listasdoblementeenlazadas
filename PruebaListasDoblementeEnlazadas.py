'''
Created on 8 nov. 2020

@author: Cesar
'''
class Nodo:
    def __init__(self, dato):
        if dato is None:
            self.__dato = int()
        else:
            self.__dato = dato 
        self.__nodoSiguiente = None
        self.__nodoAnterior = None
        
    def getDato(self):
        return self.__dato
    def setDato(self, dato):
        self.__dato=dato
    
    def getNodoSiguiente(self):
        return self.__nodoSiguiente
    def setNodoSiguiente(self, nodoSiguiente):
        self.__nodoSiguiente=nodoSiguiente
        
    def getNodoAnterior(self):
        return self.__nodoAnterior
    def setNodoAnterior(self, nodoAnterior):
        self.__nodoAnterior = nodoAnterior
        
    def __str__(self):
        return ("Nodo [nodoAnterior=" + str(self.getNodoAnterior()) + ", dato=" + str(self.getDato()) + ", nodoSiguiente=" + str(self.getNodoSiguiente()) + "]")
    
class ListaDoblementeEnlazada:
    
    def correcion(self):
        e=1
        while e==1:
            try:
                r = int(input())
            except:
                print("Ups, solo numero enteros mayores a 0")
                e=1
            else:
                if r>0:
                    e=0
                else:
                    print("Ups, solo numeros entero mayores a 0")
                    e=1
        return r

    
    
    
    
    
    def __init__(self):
        self.nodoInicio = None
        self.nodoFin = None
        
    def listaVacia(self):
        return ((self.nodoInicio==None)and(self.nodoFin==None))
    
    def agregarElementoInicio(self, dato):
        nodoNuevo = Nodo(dato)
        if self.listaVacia():
            self.nodoInicio = nodoNuevo
            self.nodoFin = Nodo
        else:
            nodoNuevo.setNodoSiguiente(self.nodoInicio)
            self.nodoInicio = nodoNuevo
     
    def agregarElementoFinal(self, dato):
        nodoNuevo = Nodo(dato)
        
        if self.listaVacia():
            self.agregarElementoInicio(dato)
        else:
            nodoActual = self.nodoInicio
            while(nodoActual.getNodoSiguiente()!=None):
                nodoActual=nodoActual.getNodoSiguiente()
            nodoActual.setNodoSiguiente(nodoNuevo)
            
    def eliminarElementoInicio(self):
        if (self.listaVacia()):
            print("No hay elementos para eliminar")
            return (-1)
        else:
            try:
                nodoActual = self.nodoInicio
                ret = nodoActual.getDato()
                self.nodoInicio = nodoActual.getNodoSiguiente()
                return ret
            except:
                return (-1)
             
    def eliminarElementoFinal(self):
        if (self.listaVacia()):
            return (-1)
        else:
            try:
                nodoAnterior = self.nodoInicio
                nodoSiguiente = self.nodoInicio.getNodoSiguiente()
                if (nodoSiguiente==None):
                    ret = self.nodoInicio.getDato()
                    self.nodoInicio=None
                    self.nodoFin=None
                    return ret
                else:
                    while(nodoSiguiente.getNodoSiguiente()!=None):
                        nodoAnterior = nodoAnterior.getNodoSiguiente()
                        nodoSiguiente = nodoSiguiente.getNodoSiguiente()
                    ret = nodoSiguiente.getDato()
                    nodoAnterior.setNodoSiguiente(None)
                    return ret
            except:
                return (-1)
            
    def eliminarElementoEspecifico(self, dato):
        if (self.nodoInicio==None):
            return (-1)
        elif((self.nodoInicio==self.nodoFin)and(self.nodoInicio.getDato()==dato)):
            print("encontrado en el primer NODO")
            n = self.nodoInicio.getDato()
            self.nodoInicio=self.nodoInicio.getNodoSiguiente()
            self.nodoFin=self.nodoInicio
            return n
        else:
            nodoAnterior = self.nodoInicio
            nodoSiguiente = self.nodoInicio.getNodoSiguiente()
            
            if ((nodoAnterior!=None)and(nodoAnterior.getDato()==dato)):
                n = nodoAnterior.getDato()
                self.nodoInicio=nodoAnterior.getNodoSiguiente();
                return n
                
            else:
                while((nodoSiguiente!=None)and(nodoSiguiente.getDato()!=dato)):
                    nodoAnterior = nodoAnterior.getNodoSiguiente()
                    nodoSiguiente = nodoSiguiente.getNodoSiguiente()
                
                if ((nodoSiguiente!=None)and(nodoSiguiente.getDato()==dato)):
                    n = nodoSiguiente.getDato()
                    nodoSiguiente = nodoSiguiente.getNodoSiguiente()
                    nodoAnterior.setNodoSiguiente(nodoSiguiente)
                    return n
                else:
                    return (-99999)
                
    def mostrarElementos(self):
        nodoActual=self.nodoInicio
        while(nodoActual!=None):
            print("<--- ["+str(nodoActual.getDato())+"] --->",end="")
            nodoActual=nodoActual.getNodoSiguiente()
        print()
            
lde = ListaDoblementeEnlazada()
opcion = 0

while(opcion!=7):
    print("=============== MENU ===============")
    print("Digite 1 para añadir dato al inicio")
    print("Digite 2 para añadir dato al final")
    print("Digite 3 para eliminar dato al inicio")
    print("Digite 4 para eliminar dato al final")
    print("Digite 5 para eliminar dato especifico")
    print("Digite 6 para mostrar la lista")
    print("Digite 7 para ***SALIR***")
    opcion = lde.correcion()
    if(opcion == 1):
        print("Ingrese el dato a insertar")
        dato = lde.correcion()
        lde.agregarElementoInicio(dato)
    elif(opcion == 2):
        print("Ingrese el dato a insertar")
        dato = lde.correcion()
        lde.agregarElementoFinal(dato)
    elif(opcion == 3):
        lde.eliminarElementoInicio()
    elif(opcion == 4):
        lde.eliminarElementoFinal()
    elif(opcion == 5):
        print("Ingresa el dato a eliminar")
        dato = lde.correcion()
        num = lde.eliminarElementoEspecifico(11)

        if(num==(-1)):
    
            print("Lista Vacia")
        elif(num==(-99999)):
            print("No se encontro el dato")
        else:
            print(" se elimino correctamente")
    elif(opcion == 6):
        lde.mostrarElementos()
    elif(opcion== 7):
        print("Gracias por usar el programa")









